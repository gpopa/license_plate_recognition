import cv2
import os

class UnsortedImages():

    def __init__(self, folderName):
        self.folderName = folderName

    def sort(self):
        directory = os.fsencode(self.folderName)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            img = cv2.imread(self.folderName+'/'+filename, 0)
            cv2.imshow("image", img)
            key = chr(cv2.waitKey(100000))
            print(key)

            #check if directory exists and if not create it
            letDirectory = os.fsencode('sorted/'+key)
            if not os.path.exists(letDirectory):
                os.makedirs(letDirectory)
            cv2.imwrite(letDirectory.decode()+'/'+filename, img)



if __name__ == '__main__':
    images = UnsortedImages('letters')
    images.sort()

