import imutils
import cv2


def resize_to_fit(image, width, height):
    """
    O functie ajutatoare pentru ajustarea imaginilor in vederea incadrarii intr-un spatiu cu dimeniunis definite
    :param image: imagine de redimensionat
    :param width: latimea dorita in pixeli
    :param height: inaltimea dorita in pixeli
    :return: imaginea redimensionata
    """

    # extragerea dimeniunilor imaginii
    (h, w) = image.shape[:2]

    # daca latime este mai mare decat inaltimea se redimensioneaza scaland latimea

    if w > h:
        image = imutils.resize(image, width=width)

    # altfel, daca inaltimea este mai mare decat latimea se redimensioneaza scaland inaltimea

    else:
        image = imutils.resize(image, height=height)

    # determinarea valorilor latimii si inaltimea pentru incadrarea in dimensiunea dorita
    padW = int((width - image.shape[1]) / 2.0)
    padH = int((height - image.shape[0]) / 2.0)

    # reaplicarea redimenionarii pentru a scapa de posibile probleme de rotunjire

    image = cv2.copyMakeBorder(image, padH, padH, padW, padW,
        cv2.BORDER_REPLICATE)
    image = cv2.resize(image, (width, height))

    # returnarea imaginii pre-procesate
    return image