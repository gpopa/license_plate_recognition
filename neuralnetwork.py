import cv2
import pickle
import os.path
import numpy as np
from imutils import paths
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers.core import Flatten, Dense
from helpers import resize_to_fit

class Network():

    def __init__(self, **kwargs):
        self.dictInfo = kwargs

    def createModelLabel(self):
        self.data, self.labels = [], []
        print(self.dictInfo["MODEL_FILENAME"])
        for imageFile in paths.list_images(self.dictInfo["LETTER_IMAGES_FOLDER"]):
            image = cv2.imread(imageFile)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            image = resize_to_fit(image, 20, 20)

            image = np.expand_dims(image, axis=2)

            label = imageFile.split(os.path.sep)[-2]

            self.data.append(image)
            self.labels.append(label)

        data = np.array(self.data, dtype="float") / 255.0
        labels = np.array(self.labels)

        (self.X_train, self.X_test, self.Y_train, self.Y_test) = train_test_split(data, labels, test_size=0.25, random_state=0)

        # Convertirea etichetelor (literele) in encodari acceptate de Keras
        lb = LabelBinarizer().fit(self.Y_train)
        self.Y_train = lb.transform(self.Y_train)
        self.Y_test = lb.transform(self.Y_test)

        with open(self.dictInfo["MODEL_LABELS_FILENAME"], "wb") as f:
            pickle.dump(lb, f)



    def build(self):
        model = Sequential()

        # Primul strat convolutional cu max pooling
        model.add(Conv2D(20, (5, 5), padding="same", input_shape=(20, 20, 1), activation="relu"))
        model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

        # Al doilea strat convolutional cu max pooling
        model.add(Conv2D(50, (5, 5), padding="same", activation="relu"))
        model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

        model.add(Flatten())
        model.add(Dense(500, activation="relu"))

        # Nivelul de iesire (contine cate un nod pentru fiecare litera/cifra pe care o putem prezice)
        model.add(Dense(15, activation="softmax"))

        # Cerere catre Keras pentru initializarea modelului TensorFlow
        model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

        # Antrenarea retelei
        model.fit(self.X_train, self.Y_train, validation_data=(self.X_test, self.Y_test), batch_size=15, epochs=10, verbose=1)

        # Salvarea modelului pe disk
        model.save(self.dictInfo["MODEL_FILENAME"])




if __name__ == '__main__':
    kwargs = {
        "LETTER_IMAGES_FOLDER" : "sorted",
        "MODEL_FILENAME" : "plates_model.hdf5",
        "MODEL_LABELS_FILENAME" : "model_labels.dat"
    }
    network = Network(**kwargs)
    network.createModelLabel()
    network.build()
