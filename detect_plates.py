from keras.models import load_model
from helpers import resize_to_fit
import numpy as np
import cv2
import pickle




class PlateRecognition():

    def __init__(self, plateFilePath):
        self.filePath = plateFilePath

    def showImage(self, img):
        cv2.imshow("image", img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()


    def prepareModel(self):
        MODEL_FILENAME = "plates_model.hdf5"
        MODEL_LABELS_FILENAME = "model_labels.dat"

        with open(MODEL_LABELS_FILENAME, "rb") as f:
            self.lb = pickle.load(f)

        self.model = load_model(MODEL_FILENAME)

    def binarize(self):

        img = cv2.imread(self.filePath, 1)
        self.showImage(img)
        img = cv2.imread(self.filePath, 0)
        self.showImage(img )
        print('Incepe binarizarea.')


        img = np.array(img, dtype=np.uint8)

        #Filtrare folsind GaussianBlur
        img = cv2.GaussianBlur(img,(5,5),0)
        self.showImage(img)

        # tehnica de  binarizare otsu cu prag otsu
        ret1, th1 = cv2.threshold(img,0,255,cv2.THRESH_OTSU)
        self.showImage(th1)


        self.binarizedImage =th1

        #self.showImage(self.binarizedImage)
        return th1

    def findContours(self):
        print("Extragere contururi.")
        if not hasattr(self, 'binarizedImage'):
            print("Imaginea trebuie binarizata inainte de extragerea contururilor, apeleaza object.binarize. ")
            return

        img = self.binarizedImage

        #Extragerea contururilor si stocarea lor in contours
        im2, contours, hierarchy = cv2.findContours(img.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        self.contours = contours

    def computePlateFromContours(self):
        print("Extragere placuta de inmatriculare")
        if not hasattr(self, 'contours'):
            print("Contururile trebuiesc extrase mai intai, apeleaza object.findContours. ")
            return
        img = self.binarizedImage
        contours = self.contours

        for contour in contours:

            (x, y, w, h) = cv2.boundingRect(contour)

            if w < h:
                continue
            if w*h < 500:
                continue
            if cv2.countNonZero(img[y:y+h,x:x+w]) < (w*h)/2:
                continue
            if w > 5*h or w < 4*h:
                continue
            break
        #croph = int(h / 11)
        #cropw = int(w / 53)

        #self.binarizedImage = img[y + croph:y + h - 2 * croph, x + 5 * cropw:x + w - cropw]
        self.binarizedImage = img[y :y + h , x :x + w]
        img = self.binarizedImage
        self.showImage(img)


    def computeRegionsFromContours(self):
        print("Extragere regiuni cu simboluri.")
        if not hasattr(self, 'contours'):
            print("Contururile trebuiesc extrase mai intai, apeleaza object.findContours. ")
            return
        img=self.binarizedImage

        height, width = img.shape


        contours = self.contours
        letter_image_regions = []

        for contour in contours:
            (x, y, w, h) = cv2.boundingRect(contour)
            if w < (width/9)*0.3 or w > (width/9)*1.25:
                continue
            if h < height*0.6:
                continue

            cv2.rectangle(img, (x, y), (x + w, y + h), (100, 100, 100), 2)
            self.showImage(img)

            letter_image_regions.append((x, y, w, h))

        #print(len(letter_image_regions))
        self.letter_image_regions = sorted(letter_image_regions, key=lambda x: x[0])

    def getExtractedLetters(self):
        print("Identificare caractere.")
        if not hasattr(self, 'letter_image_regions'):
            print("Regiunile trebuiesc calculate din contururi mai intai, apeleaza object.computeRegionsFromContours. ")
            return

        if not hasattr(self, 'model'):
            print("Modelul trebuie pregatit, apeleaza object.prepareModel. ")
            return

        model = self.model

        lb = self.lb

        img = self.binarizedImage
        letter_image_regions = self.letter_image_regions
        self.lettersExtracted = []

        for index, letter_bounding_box in enumerate(letter_image_regions):
            x, y, w, h = letter_bounding_box
            pixels = 2
            try:
                letter_image = img[y - pixels:y + h + pixels, x - pixels:x + w + pixels]
                newLetterImage = resize_to_fit(letter_image, 20, 20)
            except:
                try:
                    pixels -= 1
                    letter_image = img[y - pixels:y + h + pixels, x - pixels:x + w + pixels]
                    newLetterImage = resize_to_fit(letter_image, 60, 40)
                except:
                    pixels -= 1
                    letter_image = img[y - pixels:y + h + pixels, x - pixels:x + w + pixels]
                    newLetterImage = resize_to_fit(letter_image, 60, 40)






            savedImagePath = self.filePath.replace('plates/','')

            savedImagePath = 'letters/'+savedImagePath.replace('.jpg',str(index)+'.jpg')

            cv2.imwrite(savedImagePath, newLetterImage)

            newLetterImage = np.expand_dims(newLetterImage, axis =2)

            newLetterImage = np.expand_dims(newLetterImage, axis=0)

            prediction = model.predict(newLetterImage)

            letter = lb.inverse_transform(prediction)[0]

            self.lettersExtracted.append(letter)

        return self.lettersExtracted




