import detect_plates as LPR

def run():
        img = LPR.PlateRecognition("plates/KTA.jpg")
        img.binarize()
        img.findContours()
        img.computePlateFromContours()
        img.findContours()
        img.computeRegionsFromContours()
        img.prepareModel()
        number = img.getExtractedLetters()
        print("Numarul de inmatriculare este:", number)

        return True;

run();